#include "Arnold.h"
#include "Arnold_types.h"
#include <ai.h>

#include <iostream>

using namespace Fabric::EDK;

FABRIC_EXT_EXPORT void Arnold_ArrayExtFloat32(AtArrayResult result, UInt8IN nkeys, UInt8IN type, Float32ExtArrayIN data)
{
   result.ptr = NULL;
   
   bool valid = true;
   uint32_t required_elements;
   switch (type)
   {
   case AI_TYPE_FLOAT:
      required_elements = 1;
      break;
   case AI_TYPE_POINT:
      required_elements = 3;
      break;
   case AI_TYPE_POINT2:
      required_elements = 2;
      break;
   case AI_TYPE_VECTOR:
      required_elements = 3;
      break;
   case AI_TYPE_RGB:
      required_elements = 3;
      break;
   case AI_TYPE_RGBA:
      required_elements = 4;
      break;
   default:
      std::cerr << "[Arnold] Unsupported type for Float32 Array conversion" << std::endl;
      valid = false;
      break;
   }

   if (valid)
   {
      if (data.size() % required_elements == 0)
      {
         uint32_t nelements = data.size() / required_elements;
         AtArray* arr = (AtArray*)AiArrayConvert(nelements, nkeys, type, &data[0]);
         result.ptr = arr;
         result.nelements = arr->nelements;
         result.type = arr->type;
         result.nkeys = arr->nkeys;
      }
      else
      {
         std::cerr << "[Arnold] Incorrect size for Float32 Array conversion. Expected a multiple of " 
                        << required_elements << ", got " << data.size() << std::endl;
      }
   }
}

FABRIC_EXT_EXPORT void Arnold_ArrayExtSInt32(AtArrayResult result, UInt8IN nkeys, SInt32ExtArrayIN data)
{
   AtArray* arr = (AtArray*)AiArrayConvert(data.size(), nkeys, AI_TYPE_INT, &data[0]);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_ArrayExtUInt32(AtArrayResult result, UInt8IN nkeys, UInt32ExtArrayIN data)
{
   AtArray* arr = (AtArray*)AiArrayConvert(data.size(), nkeys, AI_TYPE_UINT, &data[0]);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_ArrayExtByte(AtArrayResult result, UInt8IN nkeys, ByteExtArrayIN data)
{
   AtArray* arr = (AtArray*)AiArrayConvert(data.size(), nkeys, AI_TYPE_BYTE, &data[0]);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_ArrayExtVec2(AtArrayResult result, UInt8IN nkeys, Vec2ExtArrayIN data)
{
   AtArray* arr = (AtArray*)AiArrayConvert(data.size(), nkeys, AI_TYPE_POINT2, &data[0]);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_ArrayExtVec3(AtArrayResult result, UInt8IN nkeys, UInt8IN type, Vec3ExtArrayIN data)
{
   if (type == AI_TYPE_VECTOR || type == AI_TYPE_POINT || type == AI_TYPE_RGB)
   {
      AtArray* arr = (AtArray*)AiArrayConvert(data.size(), nkeys, type, &data[0]);
      result.ptr = arr;
      result.nelements = arr->nelements;
      result.type = arr->type;
      result.nkeys = arr->nkeys;
   }
   else
   {
      std::cerr << "[Arnold] Unsupported type for Vec3 Array conversion" << std::endl;
      result.ptr = NULL;
   }
}

FABRIC_EXT_EXPORT void Arnold_ArrayExtColor(AtArrayResult result, UInt8IN nkeys, UInt8IN type, ColorExtArrayIN data)
{
   if (type == AI_TYPE_RGBA)
   {
      AtArray* arr = (AtArray*)AiArrayConvert(data.size(), nkeys, type, &data[0]);
      result.ptr = arr;
      result.nelements = arr->nelements;
      result.type = arr->type;
      result.nkeys = arr->nkeys;
   }
   else if (type == AI_TYPE_RGB)
   {
      // TODO: conversion here will be slow. can we do this any better?
      AtRGB* rgb = new AtRGB[data.size()];
      for (uint32_t i=0; i < data.size(); ++i)
      {
         rgb[i].r = data[i].r;
         rgb[i].g = data[i].g;
         rgb[i].b = data[i].b;
      }
      AtArray* arr = (AtArray*)AiArrayConvert(data.size(), nkeys, type, &rgb[0]);
      result.ptr = arr;
      result.nelements = arr->nelements;
      result.type = arr->type;
      result.nkeys = arr->nkeys;
      delete[] rgb;
   }
   else
   {
      std::cerr << "[Arnold] Unsupported type for Vec3 Array conversion" << std::endl;
      result.ptr = NULL;
   }
}


FABRIC_EXT_EXPORT void Arnold_ArrayAllocate(AtArrayResult result, UInt32IN nelements, UInt8IN nkeys, UInt8IN type)
{
   AtArray* arr = (AtArray*)AiArrayAllocate(nelements, nkeys, type);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_ArrayDestroy(AtArrayIO array)
{
   AiArrayDestroy((AtArray*)array.ptr);
   array.ptr = NULL;
   array.type = AI_TYPE_UNDEFINED;
   array.nelements = 0;
   array.nkeys = 0;
}

FABRIC_EXT_EXPORT void Arnold_ArrayConvert(AtArrayResult result, UInt32IN nelements, UInt8IN nkeys, UInt8IN type, 
                                             DataIN data)
{
   AtArray* arr = (AtArray*)AiArrayConvert(nelements, nkeys, type, data);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_ArrayCopy(AtArrayResult result, AtArrayIN array)
{
   AtArray* arr = (AtArray*)AiArrayCopy((AtArray*)array.ptr);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_ArraySetKey(AtArrayIO array, UInt8IN key, DataIN data)
{
   return AiArraySetKey((AtArray*)array.ptr, key, data);
}

FABRIC_EXT_EXPORT void Arnold_ArrayInterpolatePnt(Vec3Result result, AtArrayIN array, Float32IN t, UInt32IN index)
{
   AtPoint pnt = AiArrayInterpolatePnt((AtArray*)array.ptr, t, index);
   result.x = pnt.x;
   result.y = pnt.y;
   result.z = pnt.z;
}

FABRIC_EXT_EXPORT void Arnold_ArrayInterpolateVec(Vec3Result result, AtArrayIN array, Float32IN t, UInt32IN index)
{
   AtVector pnt = AiArrayInterpolateVec((AtArray*)array.ptr, t, index);
   result.x = pnt.x;
   result.y = pnt.y;
   result.z = pnt.z;
}

FABRIC_EXT_EXPORT void Arnold_ArrayInterpolateRGB(ColorResult result, AtArrayIN array, Float32IN t, UInt32IN index)
{
   AtRGB rgb = AiArrayInterpolateRGB((AtArray*)array.ptr, t, index);
   result.r = rgb.r;
   result.g = rgb.g;
   result.b = rgb.b;
   result.a = 1.0f;
}

FABRIC_EXT_EXPORT void Arnold_ArrayInterpolateRGBA(ColorResult result, AtArrayIN array, Float32IN t, UInt32IN index)
{
   AtRGBA rgb = AiArrayInterpolateRGBA((AtArray*)array.ptr, t, index);
   result.r = rgb.r;
   result.g = rgb.g;
   result.b = rgb.b;
   result.a = rgb.a;
}

FABRIC_EXT_EXPORT KL::Float32 Arnold_ArrayInterpolateFlt(AtArrayIN array, Float32IN t, UInt32IN index)
{
   return AiArrayInterpolateFlt((AtArray*)array.ptr, t, index);
}

FABRIC_EXT_EXPORT void Arnold_ArrayInterpolateMtx(Mat44Result result, AtArrayIN array, Float32IN t, UInt32IN index)
{
   AtMatrix mtx;
   AiArrayInterpolateMtx((AtArray*)array.ptr, t, index, mtx);
   memcpy(&result.row0, mtx[0], sizeof(float)*4);
   memcpy(&result.row1, mtx[1], sizeof(float)*4);
   memcpy(&result.row2, mtx[2], sizeof(float)*4);
   memcpy(&result.row3, mtx[3], sizeof(float)*4);
}

FABRIC_EXT_EXPORT KL::Boolean Arnold_ArrayGetBool(AtArrayIN array, UInt32IN idx)
{
   return AiArrayGetBool((AtArray*)array.ptr, idx);
}

FABRIC_EXT_EXPORT KL::SInt32 Arnold_ArrayGetInt(AtArrayIN array, UInt32IN idx)
{
   return AiArrayGetInt((AtArray*)array.ptr, idx);
}

FABRIC_EXT_EXPORT KL::UInt32 Arnold_ArrayGetUInt(AtArrayIN array, UInt32IN idx)
{
   return AiArrayGetUInt((AtArray*)array.ptr, idx);
}

FABRIC_EXT_EXPORT KL::Byte Arnold_ArrayGetByte(AtArrayIN array, UInt32IN idx)
{
   return AiArrayGetByte((AtArray*)array.ptr, idx);
}

FABRIC_EXT_EXPORT KL::Float32 Arnold_ArrayGetFlt(AtArrayIN array, UInt32IN idx)
{
   return AiArrayGetFlt((AtArray*)array.ptr, idx);
}

FABRIC_EXT_EXPORT KL::Data Arnold_ArrayGetPtr(AtArrayIN array, UInt32IN idx)
{
   return AiArrayGetPtr((AtArray*)array.ptr, idx);
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetRGB(ColorResult result, AtArrayIN array, UInt32IN idx)
{
   AtRGB rgb = AiArrayGetRGB((AtArray*)array.ptr, idx);
   result.r = rgb.r;
   result.g = rgb.g;
   result.b = rgb.b;
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetRGBA(ColorResult result, AtArrayIN array, UInt32IN idx)
{
   AtRGBA rgb = AiArrayGetRGBA((AtArray*)array.ptr, idx);
   result.r = rgb.r;
   result.g = rgb.g;
   result.b = rgb.b;
   result.a = 1.0f;
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetPnt(Vec3Result result, AtArrayIN array, UInt32IN idx)
{
   AtPoint pnt = AiArrayGetPnt((AtArray*)array.ptr, idx);
   result.x = pnt.x;
   result.y = pnt.y;
   result.z = pnt.z;
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetPnt2(Vec2Result result, AtArrayIN array, UInt32IN idx)
{
   AtPoint2 pnt = AiArrayGetPnt2((AtArray*)array.ptr, idx);
   result.x = pnt.x;
   result.y = pnt.y;
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetVec(Vec3Result result, AtArrayIN array, UInt32IN idx)
{
   AtVector pnt = AiArrayGetVec((AtArray*)array.ptr, idx);
   result.x = pnt.x;
   result.y = pnt.y;
   result.z = pnt.z;
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetStr(StringResult result, AtArrayIN array, UInt32IN idx)
{
   result = AiArrayGetStr((AtArray*)array.ptr, idx);
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetMtx(Mat44Result result, AtArrayIN array, UInt32IN idx)
{
   AtMatrix mtx;
   AiArrayGetMtx((AtArray*)array.ptr, idx, mtx);
   memcpy(&result.row0, mtx[0], sizeof(float)*4);
   memcpy(&result.row1, mtx[1], sizeof(float)*4);
   memcpy(&result.row2, mtx[2], sizeof(float)*4);
   memcpy(&result.row3, mtx[3], sizeof(float)*4);
}

FABRIC_EXT_EXPORT void Arnold_ArrayGetArray(AtArrayResult result, AtArrayIN array, UInt32IN idx)
{
   AtArray* arr = (AtArray*)AiArrayGetArray((AtArray*)array.ptr, idx);
   result.ptr = arr;
   result.nelements = arr->nelements;
   result.type = arr->type;
   result.nkeys = arr->nkeys;
}

FABRIC_EXT_EXPORT void Arnold_ArraySetBool(AtArrayIO array, UInt32IN idx, BooleanIN val)
{
   AiArraySetBool((AtArray*)array.ptr, idx, val);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetByte(AtArrayIO array, UInt32IN idx, ByteIN val)
{
   AiArraySetByte((AtArray*)array.ptr, idx, val);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetInt(AtArrayIO array, UInt32IN idx, SInt32IN val)
{
   AiArraySetInt((AtArray*)array.ptr, idx, val);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetUInt(AtArrayIO array, UInt32IN idx, UInt32IN val)
{
   AiArraySetUInt((AtArray*)array.ptr, idx, val);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetFlt(AtArrayIO array, UInt32IN idx, Float32IN val)
{
   AiArraySetFlt((AtArray*)array.ptr, idx, val);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetRGB(AtArrayIO array, UInt32IN idx, ColorIN val)
{
   AtRGB rgb = AiColor((float)val.r, (float)val.g, (float)val.b);
   AiArraySetRGB((AtArray*)array.ptr, idx, rgb);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetRGBA(AtArrayIO array, UInt32IN idx, ColorIN val)
{
   AtRGBA rgba;
   rgba.r = val.r;
   rgba.g = val.g;
   rgba.b = val.b;
   rgba.a = val.a;
   AiArraySetRGBA((AtArray*)array.ptr, idx, rgba);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetPnt(AtArrayIO array, UInt32IN idx, Vec3IN val)
{
   AtPoint pnt = AiVector((float)val.x, (float)val.y, (float)val.z);
   AiArraySetPnt((AtArray*)array.ptr, idx, pnt);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetPnt2(AtArrayIO array, UInt32IN idx, Vec2IN val)
{
   AtPoint2 pnt = AiVector2((float)val.x, (float)val.y);
   AiArraySetPnt2((AtArray*)array.ptr, idx, pnt);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetVec(AtArrayIO array, UInt32IN idx, Vec3IN val)
{
   AtVector pnt = AiVector((float)val.x, (float)val.y, (float)val.z);
   AiArraySetVec((AtArray*)array.ptr, idx, pnt);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetStr(AtArrayIO array, UInt32IN idx, StringIN str)
{
   AiArraySetStr((AtArray*)array.ptr, idx, str.data());
}

FABRIC_EXT_EXPORT void Arnold_ArraySetPtr(AtArrayIO array, UInt32IN idx, DataIN ptr)
{
   AiArraySetPtr((AtArray*)array.ptr, idx, ptr);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetArray(AtArrayIO array, UInt32IN idx, AtArrayIN val)
{
   AiArraySetArray((AtArray*)array.ptr, idx, (AtArray*)val.ptr);
}

FABRIC_EXT_EXPORT void Arnold_ArraySetMtx(AtArrayIO array, UInt32IN idx, Mat44IN matrix)
{
   AtMatrix mtx = {
      {matrix.row0.x, matrix.row0.y, matrix.row0.z, matrix.row0.t},
      {matrix.row1.x, matrix.row1.y, matrix.row1.z, matrix.row1.t},
      {matrix.row2.x, matrix.row2.y, matrix.row2.z, matrix.row2.t},
      {matrix.row3.x, matrix.row3.y, matrix.row3.z, matrix.row3.t},
   };
   AiArraySetMtx((AtArray*)array.ptr, idx, mtx);
}


FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean ArnoldArrayConvertExtFloat32(AtArrayIN array, Float32ExtArrayIO data)
{
   AtArray* arr = (AtArray*)array.ptr;

   int nelements;
   switch (arr->type)
   {
   case AI_TYPE_FLOAT:
      nelements = arr->nelements;
      break;
   case AI_TYPE_VECTOR:
      nelements = arr->nelements * 3;
      break;
   case AI_TYPE_POINT2:
      nelements = arr->nelements * 2;
      break;
   case AI_TYPE_POINT:
      nelements = arr->nelements * 3;
      break;
   default:
      return false;
   }
   data = Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Float32>((float*)arr->data, nelements);
   return true;
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean ArnoldArrayConvertExtVec3(AtArrayIN array, Vec3ExtArrayIO data)
{
   AtArray* arr = (AtArray*)array.ptr;

   int nelements;
   switch (arr->type)
   {
   case AI_TYPE_FLOAT:
      nelements = arr->nelements / 3;
      break;
   case AI_TYPE_VECTOR:
      nelements = arr->nelements;
      break;
   case AI_TYPE_POINT:
      nelements = arr->nelements;
      break;
   default:
      return false;
   }
   data = Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Vec3>((Fabric::EDK::KL::Vec3*)arr->data, nelements);
   return true;
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean ArnoldArrayConvertExtVec2(AtArrayIN array, Vec2ExtArrayIO data)
{
   AtArray* arr = (AtArray*)array.ptr;

   int nelements;
   switch (arr->type)
   {
   case AI_TYPE_FLOAT:
      nelements = arr->nelements / 2;
      break;
   case AI_TYPE_POINT2:
      nelements = arr->nelements;
      break;
   default:
      return false;
   }
   data = Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Vec2>((Fabric::EDK::KL::Vec2*)arr->data, nelements);
   return true;
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Boolean ArnoldArrayConvertExtUInt32(AtArrayIN array, UInt32ExtArrayIO data)
{
   AtArray* arr = (AtArray*)array.ptr;

   int nelements;
   switch (arr->type)
   {
   case AI_TYPE_UINT:
      nelements = arr->nelements;
      break;
   default:
      return false;
   }
   data = Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::UInt32>((Fabric::EDK::KL::UInt32*)arr->data, nelements);
   return true;
}



