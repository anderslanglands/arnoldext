#include "Arnold.h"
#include "Arnold_types.h"

#include <ai.h>

FABRIC_EXT_EXPORT void Arnold_MsgSetLogFileName(StringIN filename)
{
   AiMsgSetLogFileName(filename.data());
}

FABRIC_EXT_EXPORT void Arnold_MsgSetLogFileFlags(SInt32IN flags)
{
   AiMsgSetLogFileFlags(flags);
}

FABRIC_EXT_EXPORT void Arnold_MsgSetConsoleFlags(SInt32IN flags)
{
   AiMsgSetConsoleFlags(flags);
}

FABRIC_EXT_EXPORT void Arnold_MsgSetMaxWarnings(SInt32IN max_warnings)
{
   AiMsgSetMaxWarnings(max_warnings);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::UInt64 Arnold_MsgUtilGetUsedMemory()
{
   return AiMsgUtilGetUsedMemory();
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::UInt32 Arnold_MsgUtilGetElapsedTime()
{
   return AiMsgUtilGetElapsedTime();
}