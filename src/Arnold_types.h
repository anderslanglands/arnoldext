#include "Arnold.h"

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::UInt32>::INParam UInt32IN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::UInt8>::INParam UInt8IN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Boolean>::INParam BooleanIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Byte>::INParam ByteIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::SInt32>::INParam SInt32IN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Float32>::INParam Float32IN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Data>::INParam DataIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::String>::INParam StringIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Mat44>::INParam Mat44IN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Color>::INParam ColorIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Vec3>::INParam Vec3IN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Vec2>::INParam Vec2IN;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::String>::Result StringResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Vec2>::Result Vec2Result;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Vec3>::Result Vec3Result;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Color>::Result ColorResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Mat44>::Result Mat44Result;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::Box3>::Result Box3Result;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Float32> >::INParam Float32ExtArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::SInt32> >::INParam SInt32ExtArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::UInt32> >::INParam UInt32ExtArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Byte> >::INParam ByteExtArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Vec3> >::INParam Vec3ExtArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Vec2> >::INParam Vec2ExtArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Color> >::INParam ColorExtArrayIN;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Float32> >::IOParam Float32ExtArrayIO;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::SInt32> >::IOParam SInt32ExtArrayIO;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::UInt32> >::IOParam UInt32ExtArrayIO;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Byte> >::IOParam ByteExtArrayIO;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Vec3> >::IOParam Vec3ExtArrayIO;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Vec2> >::IOParam Vec2ExtArrayIO;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::ExternalArray<Fabric::EDK::KL::Color> >::IOParam ColorExtArrayIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::VariableArray<Fabric::EDK::KL::Float32> >::INParam Float32VarArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::VariableArray<Fabric::EDK::KL::SInt32> >::INParam SInt32VarArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::VariableArray<Fabric::EDK::KL::UInt32> >::INParam UInt32VarArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::VariableArray<Fabric::EDK::KL::Byte> >::INParam ByteVarArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::VariableArray<Fabric::EDK::KL::Vec3> >::INParam Vec3VarArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::VariableArray<Fabric::EDK::KL::Vec2> >::INParam Vec2VarArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::VariableArray<Fabric::EDK::KL::Color> >::INParam ColorVarArrayIN;


typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtArray>::Result AtArrayResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtArray>::INParam AtArrayIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtArray>::IOParam AtArrayIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNode>::Result AtNodeResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNode>::INParam AtNodeIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNode>::IOParam AtNodeIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeEntry>::Result AtNodeEntryResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeEntry>::INParam AtNodeEntryIN;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtAOVEntry>::Result AtAOVEntryResult;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtUserParamEntry>::Result AtUserParamEntryResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtUserParamEntry>::INParam AtUserParamEntryIN;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtParamEntry>::Result AtParamEntryResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtParamEntry>::INParam AtParamEntryIN;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtParamValue>::Result AtParamValueResult;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtUserParamIterator>::Result AtUserParamIteratorResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtUserParamIterator>::INParam AtUserParamIteratorIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtUserParamIterator>::IOParam AtUserParamIteratorIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeIterator>::Result AtNodeIteratorResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeIterator>::INParam AtNodeIteratorIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeIterator>::IOParam AtNodeIteratorIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeEntryIterator>::Result AtNodeEntryIteratorResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeEntryIterator>::INParam AtNodeEntryIteratorIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeEntryIterator>::IOParam AtNodeEntryIteratorIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtAOVIterator>::Result AtAOVIteratorResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtAOVIterator>::INParam AtAOVIteratorIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtAOVIterator>::IOParam AtAOVIteratorIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtParamIterator>::Result AtParamIteratorResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtParamIterator>::INParam AtParamIteratorIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtParamIterator>::IOParam AtParamIteratorIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtMetaDataIterator>::Result AtMetaDataIteratorResult;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtMetaDataIterator>::INParam AtMetaDataIteratorIN;
typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtMetaDataIterator>::IOParam AtMetaDataIteratorIO;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtMetaDataEntry>::Result AtMetaDataEntryResult;

typedef Fabric::EDK::KL::Traits<Fabric::EDK::KL::AtNodeMethods>::INParam AtNodeMethodsIN;