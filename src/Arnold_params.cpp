#include "Arnold.h"

#include <ai_params.h>

#include "Arnold_types.h"

FABRIC_EXT_EXPORT void Arnold_UserParamGetName(StringResult result, AtUserParamEntryIN upentry)
{
   result = AiUserParamGetName((AtUserParamEntry*)upentry.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Integer Arnold_UserParamGetType(AtUserParamEntryIN upentry)
{
   return AiUserParamGetType((AtUserParamEntry*)upentry.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Integer Arnold_UserParamGetArrayType(AtUserParamEntryIN upentry)
{
   return AiUserParamGetArrayType((AtUserParamEntry*)upentry.ptr);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::Integer Arnold_UserParamGetCategory(AtUserParamEntryIN upentry)
{
   return AiUserParamGetCategory((AtUserParamEntry*)upentry.ptr);
}