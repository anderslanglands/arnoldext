#include "Arnold.h"
#include "Arnold_types.h"

#include <ai.h>

FABRIC_EXT_EXPORT Fabric::EDK::KL::SInt32 Arnold_ASSWrite(StringIN filename, SInt32IN mask, BooleanIN open_procs, BooleanIN binary)
{
   return AiASSWrite(filename.data(), mask, open_procs, binary);
}

FABRIC_EXT_EXPORT Fabric::EDK::KL::SInt32 Arnold_ASSLoad(StringIN filename, SInt32IN mask)
{
   return AiASSLoad(filename.data(), mask);  
}