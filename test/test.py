import os
if 'FABRIC_EXTS_PATH' in os.environ:
  os.environ['FABRIC_EXTS_PATH'] = os.pathsep.join(['.', os.environ['FABRIC_EXTS_PATH']])
else:
  os.environ['FABRIC_EXTS_PATH'] = '.'

import FabricEngine.Core as fabric
fabricClient = fabric.createClient();

opSource = """
require Arnold;

operator entry() {
  report("KL: Enter entry");
  AiBegin();
  AiEnd();
}
"""
op = fabricClient.DG.createOperator("op")
op.setSourceCode(opSource)
op.setEntryPoint("entry")

b = fabricClient.DG.createBinding()
b.setOperator(op)
b.setParameterLayout([])

node = fabricClient.DG.createNode("node")
node.bindings.append(b)
node.evaluate()

fabricClient.close()
